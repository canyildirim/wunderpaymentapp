#Wunder Payment Web App
## Installation

Go to project directory
```$xslt
docker compose up -d # if mysql doesnt exist on local
composer install 
php bin/console doctrine:database:create #creating local db
php bin/console doctrine:migrations:migrate #creating tables on local
./bin/console server:run # run localhost
``` 

## Used Techs

PHP, Symfony4, Bootstrap CSS.


## What could have been done better ?

- A key-value tool can be used for sessions so that we wouldn't be consuming diskspace. It could be problem when we scale our app
- A timestamp columns should have been added to the all tables
- The payment request could have been sent by AJAX request so that user would feel faster experience
