<?php
/**
 * Created by PhpStorm.
 * User: cyildirim
 * Date: 10/6/18
 * Time: 1:15 PM
 */

namespace App\Service;


class PaymentProvider
{
    private $PAYMENT_URL = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";

    public function placeOrder($customerId,$iban,$owner)
    {
        $data = array(
            'customerId' => $customerId,
            'iban' => $iban,
            'owner' => $owner
        );

        $payload = json_encode($data);

        $ch = curl_init($this->PAYMENT_URL);
        curl_setopt($ch, CURLPROTO_HTTPS, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
        curl_setopt($ch, CURLOPT_POSTFIELDS,$payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //execute the POST request
        $result = curl_exec($ch);

        //close cURL resource
        curl_close($ch);

        $json = json_decode($result,true);

        return $json['paymentDataId'];

    }

}