<?php
/**
 * Created by PhpStorm.
 * User: cyildirim
 * Date: 10/6/18
 * Time: 1:53 AM
 */

namespace App\Controller;


use App\Entity\Address;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MainController
 * @package App\Controller
 */
class MainController extends AbstractController
{


    /**
     * @Route("/", name="index")
     */
    public function index(SessionInterface $session)
    {

        switch ($session->get('step')){
            case "address":
                $redirectResponse =  $this->redirect("/address");
                break;
            case "payment":
                $redirectResponse =  $this->redirect("/payment");
                break;
            case "payment_success":
                $redirectResponse =  $this->redirect("/payment_success");
                break;
            default:
                $redirectResponse =  $this->redirect("/user");
                break;

        }

       return $redirectResponse;
    }

    /**
     * @Route("/user", name="user")
     */
    public function user(SessionInterface $session,Request $request)
    {
        $session->start();
        $paymentFail = false;
        if ($session->get('step') == 'payment_failed'){
            $paymentFail = true;
        }
        //step1
        // creates a task and gives it some dummy data for this example
        $user = new User();


        $form = $this->createFormBuilder($user)
            ->add('firstname', TextType::class,array('attr'=> array('class' => 'form-control')))
            ->add('lastname', TextType::class,array('attr'=> array('class' => 'form-control')))
            ->add('phone', TelType::class,array('attr'=> array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'save & insert address', 'attr' => array('class'=> 'btn form-control btn-primary')))
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $session->set('firstname',$form->get('firstname')->getData());
            $session->set('lastname',$form->get('lastname')->getData());
            $session->set('phone',$form->get('phone')->getData());
            $session->set('step','address');
            $session->save();
            return $this->redirect('/');
        }

        return $this->render('userregister.html.twig', array(
            'form' => $form->createView(),
            'payment_fail' => $paymentFail
        ));
    }

    /**
     * @Route("/address",name="address")
     * @param SessionInterface $session
     * @param Request $request
     */
    public function address(SessionInterface $session,Request $request){
        $firstName = $session->get('firstname');
        $address = new Address();

        $form = $this->createFormBuilder($address)
            ->add('street', TextType::class,array('attr'=> array('class' => 'form-control')))
            ->add('house_number', TextType::class,array('attr'=> array('class' => 'form-control')))
            ->add('zipcode', TelType::class,array('attr'=> array('class' => 'form-control')))
            ->add('city', TextType::class,array('attr'=> array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'save & insert payment info', 'attr' => array('class'=> 'btn form-control btn-primary')))
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $session->set('street',$form->get('street')->getData());
            $session->set('house_number',$form->get('house_number')->getData());
            $session->set('zipcode',$form->get('zipcode')->getData());
            $session->set('city',$form->get('city')->getData());
            $session->set('step','payment');
            $session->save();
            return $this->redirect('/');
        }

        return $this->render('addressinsert.html.twig', array(
            'form' => $form->createView(),
            'firstname' => $firstName
        ));
    }




}