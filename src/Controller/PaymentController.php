<?php
/**
 * Created by PhpStorm.
 * User: cyildirim
 * Date: 10/6/18
 * Time: 9:55 PM
 */

namespace App\Controller;



use App\Entity\Address;
use App\Entity\Payment;
use App\Entity\User;
use App\Repository\AddressRepository;
use App\Repository\PaymentRepository;
use App\Repository\UserRepository;
use App\Service\PaymentProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


class PaymentController extends AbstractController
{


    /**
     * @Route("/payment", name="payment")
     * @param SessionInterface $session
     * @param Request $request
     */
    public function payment(
        SessionInterface $session,
        Request $request,
        PaymentProvider $paymentProvider,
        UserRepository $userRepository,
        AddressRepository $addressRepository,
        PaymentRepository $paymentRepository
    )
    {
        $firstName = $session->get('firstname');

        $payment = new Payment();

        $form = $this->createFormBuilder($payment)
            ->add('account_owner', TextType::class,array('attr'=> array('class' => 'form-control')))
            ->add('iban', TextType::class,array('attr'=> array('class' => 'form-control')))
            ->add('save', SubmitType::class, array('label' => 'place order', 'attr' => array('class'=> 'btn form-control btn-primary')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = new User();
            $user
                ->setFirstname($session->get('firstname'))
                ->setLastname($session->get('lastname'))
                ->setPhone($session->get('phone'));

            $userId = $userRepository->saveUser($user);
            dump($userId);

            $address = new Address();
            $address
                ->setUserId($userId)
                ->setStreet($session->get('street'))
                ->setHouseNumber($session->get('house_number'))
                ->setZipcode($session->get('zipcode'))
                ->setCity($session->get('city'));
            $addressRepository->saveAddress($address);

            $iban = $form->get('iban')->getData();
            $accountOwner = $form->get('account_owner')->getData();

            $paymentDataId = $paymentProvider->placeOrder($userId,$iban,$accountOwner);

            if (isset($paymentDataId)){
                $payment = new Payment();
                $payment
                    ->setUserId($userId)
                    ->setAccountOwner($accountOwner)
                    ->setIban($iban)
                    ->setStatus("success")
                    ->setPaymentDataId($paymentDataId);
                $paymentRepository->savePayment($payment);

                $session->set('payment_data_id', $paymentDataId);
                $session->set('step','payment_success');
                $session->save();
                return $this->redirect('/');
            }else{
                $session->clear();
                $session->set('step','payment_failed');
                $this->redirect("/");
            }

        }

        return $this->render('payment.html.twig', array(
            'form' => $form->createView(),
            'firstname' => $firstName
        ));
    }


    /**
     * @Route("/payment_success", name="payment_success")
     * @param SessionInterface $session
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function paymentSuccess(SessionInterface $session,
                                   Request $request){
        $paymentDataId = $session->get('payment_data_id');
        dump($paymentDataId);

        if (is_null($paymentDataId)){
            return $this->redirect("/");
        }

        $fullName = $session->get('firstname')." ".$session->get('lastname');
        $session->clear();

        return $this->render('payment_success.html.twig',array(
            'payment_id' => $paymentDataId,
            'fullname' => $fullName
        ));
    }
}